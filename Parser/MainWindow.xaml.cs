﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Web;
using System.Net;

namespace Parser
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int pageIndex; //номер страницы
        int maxPage; //макс. кол-во страниц
        int threatsPerPage = 15; //записей на стр.
        int countBeg; //первая запись на текущей странице
        int countEnd; //последняя запись на текущей странице

        private enum PagingMode { First = 1, Next = 2, Previous = 3 };
        private List<Threat> threatsList = new List<Threat>();


        public MainWindow()
        {
            InitializeComponent();
        }

        

        private static List<Threat> GetThreatList()
        {
            Excel.Application excelApp = new Excel.Application();
            //путь до файла:
            var thrListLocation = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "thrlist.xlsx");
            //здесь сделать проверку
            if (File.Exists(thrListLocation))
            {
                excelApp.Workbooks.Open(thrListLocation); //открываю книгу
            }
            else
            {
                var res = MessageBoxes.DownLoadMessage();
                if (res == MessageBoxResult.OK)
                {
                    GetFileFromWeb(thrListLocation);
                }
                else
                {
                    return null;
                }
            }

            excelApp.Workbooks.Open(thrListLocation); //открываю книгу

            //Коллекция угроз:
            List<Threat> threatList = new List<Threat>();
            Excel.Worksheet currentSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];
            //считываю угрозы в коллекцию:
            int row = 3;
            //пока не наткнёмся на пустую ячейку:
            while (currentSheet.get_Range("A" + row.ToString()).Value2 != null)
            {
                ArrayList tempList = new ArrayList(); //коллекция с параметрами одной угрозы
                for (char column = 'A'; column < 'I'; column++)
                {
                    Excel.Range cell = currentSheet.get_Range(column.ToString() + row.ToString());
                    tempList.Add(cell.Value2.ToString());
                }
                threatList.Add(new Threat(tempList));
                row++;
            }
            excelApp.Workbooks.Close();
            excelApp.Quit();

            return threatList;
        }

        

        private static void GetFileFromWeb(string location)
        {
            string threatsURL = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
            WebClient wc = new WebClient();
            wc.DownloadFile(threatsURL, location);
            wc.Dispose(); //!!!!!!!!
        }

        private void DownloadButton_Click(object sender, RoutedEventArgs e)
        {
            threatsList = GetThreatList();
            if (threatsList != null)
            {
                Navigate((int)PagingMode.First);
                UpdateButton.IsEnabled = true;
                SaveButton.IsEnabled = true;
                txtInfoBlock.Text = "Подробная информация об угрозе доступна по двойному щелчку мыши.";
            }
        }


        private void Navigate(int pageMode)
        {
            switch (pageMode)
            {
                case (int)PagingMode.First: //начало
                    countBeg = 0;
                    countEnd = 0;
                    pageIndex = 0;
                    dataGrid.ItemsSource = threatsList.Take(threatsPerPage);
                                            //если последняя страница окажется пустой:
                    if (threatsList.Count % threatsPerPage == 0)
                    {
                        maxPage = threatsList.Count / threatsPerPage - 2;
                    }
                    else
                    {
                        maxPage = threatsList.Count / threatsPerPage - 1;
                    }
                    prevPageButton.IsEnabled = false;
                    nextPageButton.IsEnabled = true;
                    break;

                case (int)PagingMode.Next: //Следующая
                    prevPageButton.IsEnabled = true;
                    if (pageIndex >= maxPage) //если выводим последнюю страницу
                    {
                        nextPageButton.IsEnabled = false;
                    }
                    pageIndex++;
                    break;
                case (int)PagingMode.Previous:
                    nextPageButton.IsEnabled = true;
                    if (pageIndex <= 1)
                    {
                        prevPageButton.IsEnabled = false;
                    }
                    pageIndex--;
                    break;
            }
            //выводим в датагрид:
            dataGrid.ItemsSource = threatsList.Skip(threatsPerPage * pageIndex).Take(threatsPerPage);
            //выводим в label инфу о страницах:
            countBeg = ((pageIndex - 1) * threatsPerPage) + threatsList.Skip((pageIndex - 1) * threatsPerPage).Take(threatsPerPage).Count() + 1; //первая на странице
            countEnd = (pageIndex * threatsPerPage) + (threatsList.Skip(pageIndex * threatsPerPage).Take(threatsPerPage)).Count(); //последняя на странице
            lblPagesInfo.Content = countBeg + " - " + countEnd + " из " + threatsList.Count;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //для вывода отдельной угрозы
        }

        private void dataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string headername = e.Column.Header.ToString();

            //Отменяем колонки, которые не будем выводить:
            if (headername == "Description" || headername == "Source" || headername == "ImpactObject" || headername == "ConfidentialityBreach" || headername == "IntegrityBreach" || headername == "AccessibilityBreach")
            {
                e.Cancel = true;
            }
            else if (headername == "Name")
            {
                e.Column.Header = "Наименование угрозы";
            }
        }

        private void NextPage_Click(object sender, RoutedEventArgs e)
        {
            Navigate((int)PagingMode.Next);
        }

        private void prevPageButton_Click(object sender, RoutedEventArgs e)
        {
            Navigate((int)PagingMode.Previous);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            //List<Threat> oldList = new List<Threat>();
            //oldList = threatsList;
            List<Threat> oldList = threatsList;
            var thrListLocation = Directory.GetCurrentDirectory();
            try
            {
                string threatsURL = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
                WebClient wc = new WebClient();
                string fileName = "thrList.xlsx";
                File.Delete(fileName);
                wc.DownloadFile(threatsURL, fileName);
                threatsList = GetThreatList();
                Navigate((int)PagingMode.First);
                MessageBoxes.UpdateReportMessageSuccess();
            }
            catch (Exception excep)
            {
                MessageBoxes.UpdateReportMessageError(excep.Message);
            }


            string changes = "";
            int changesCount = 0;
            for (int i = 0; i < Math.Min(oldList.Count, threatsList.Count); i++)
            {
                if (oldList[i].Equals(threatsList[i])) //если сравниваем угрозы с одним ID
                {
                    string temp = oldList[i].EqualParams(threatsList[i]); //то в строку с изменениями вводим изменения
                    if (temp != "")
                    {
                        changes += temp;
                        changesCount++;
                    }
                }
            }
            if (changesCount == 0)
            {
                MessageBoxes.UpdateInformationMessage("Изменений не произошло");
            }
            else
            {
                MessageBoxes.UpdateInformationMessage("Что-то изменилось в " + changesCount + " угрозах\n\nСписок изменений:\n" + changes);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string fileName = "thrList_saved.xlsx";
            Excel.Application excelApp = new Excel.Application();
            //путь до файла:
            var thrListLocation = System.IO.Path.Combine(Directory.GetCurrentDirectory(), fileName);
            //здесь сделать проверку
            if (File.Exists(thrListLocation))
            {
                File.Delete(fileName);
            }

            //excelApp.Workbooks.Open(thrListLocation); //открываю книгу

            Excel.Workbooks currentBooks = excelApp.Workbooks;
            Excel.Workbook currentBook = currentBooks.Add();
            Excel.Worksheet currentSheet = currentBook.Worksheets[1];


            // добавляем в книгу заголовки и делаем некоторым нормальную ширину:
            currentSheet.Cells[2, 1] = "ID угрозы";
            currentSheet.Columns[1].ColumnWidth = 10;

            currentSheet.Cells[2, 2] = "Наименование угрозы";
            currentSheet.Columns[2].ColumnWidth = 90;

            currentSheet.Cells[2, 3] = "Описание";
            currentSheet.Columns[3].ColumnWidth = 90;

            currentSheet.Cells[2, 4] = "Источник угрозы";
            currentSheet.Cells[2, 5] = "Объект воздействия";
            currentSheet.Cells[2, 6] = "Нарушение конфиденциальности";
            currentSheet.Cells[2, 7] = "Нарушение целостности";
            currentSheet.Cells[2, 8] = "Нарушение доступности";

            //заголовки жирные:
            currentSheet.Cells[2, 1].EntireRow.Font.Bold = true;

            int rowList = 0; //ряд коллекции
            for (int row = 3; row < threatsList.Count+3; row++) //ряд экселя
            {
                currentSheet.get_Range("A" + row.ToString()).Value = threatsList[rowList].ID;
                currentSheet.get_Range("B" + row.ToString()).Value = threatsList[rowList].Name;
                currentSheet.get_Range("C" + row.ToString()).Value = threatsList[rowList].Description;
                currentSheet.get_Range("D" + row.ToString()).Value = threatsList[rowList].Source;
                currentSheet.get_Range("E" + row.ToString()).Value = threatsList[rowList].ImpactObject;
                currentSheet.get_Range("F" + row.ToString()).Value = threatsList[rowList].ConfidentialityBreach;
                currentSheet.get_Range("G" + row.ToString()).Value = threatsList[rowList].IntegrityBreach;
                currentSheet.get_Range("H" + row.ToString()).Value = threatsList[rowList].AccessibilityBreach;
                rowList++;
            }

            //высоту столбцов нормальную делаем:
            currentSheet.Rows.RowHeight = 15;

            //сохраняем и говорим, что всё ок:
            excelApp.Workbooks[1].SaveAs(thrListLocation);
            excelApp.Workbooks.Close();
            excelApp.Quit();
            MessageBoxes.SaveSuccessMessage(thrListLocation);
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBoxes.ThreatInformationMessage(dataGrid.CurrentCell.Item.ToString());
        }
    }
}
