﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Net;

namespace Parser
{
    class MessageBoxes
    {
        internal static MessageBoxResult DownLoadMessage()
        {
            string messageText = "На Вашем компьютере не найден файл\"thrlist.xlsx\". Скачать его из Интернета?";
            string caption = "Download Message";
            MessageBoxButton button = MessageBoxButton.OKCancel;
            MessageBoxImage icon = MessageBoxImage.Question;
            MessageBoxResult res = MessageBox.Show(messageText, caption, button, icon);
            return res;
        }

        internal static void UpdateReportMessageSuccess()
        {
            string messageText = "База данных угроз безопасности информации успешно обновлена!";
            string caption = "Update Report";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Information;
            MessageBox.Show(messageText, caption, button, icon);
        }

        internal static void UpdateReportMessageError(string errorMessage)
        {
            string messageText = "Ошибка обновления базы данных угроз безопасности информации :(\n\nСообщение об ошибке: " + errorMessage;
            string caption = "Update Report";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Error;
            MessageBox.Show(messageText, caption, button, icon);
        }
        internal static void UpdateInformationMessage(string report)
        {
            string messageText = report;
            string caption = "Update Report";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Asterisk;
            MessageBox.Show(messageText, caption, button, icon);
        }

        internal static void ThreatInformationMessage(string threat)
        {
            string messageText = threat;
            string caption = "Threat Information";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Information;
            MessageBox.Show(messageText, caption, button, icon);
        }

        internal static void SaveSuccessMessage(string location)
        {
            string messageText = "База успешно сохранена в следующий путь: " + location;
            string caption = "Save Message";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Information;
            MessageBox.Show(messageText, caption, button, icon);
        }
    }
}
