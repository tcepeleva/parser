﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    class Threat
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string ImpactObject { get; set; }
        public string ConfidentialityBreach { get; set; }
        public string IntegrityBreach { get; set; }
        public string AccessibilityBreach { get; set; }
        //public Threat(int id, string name, string description, string source, string impObj, bool cb, bool ib, bool ab)
        //{
        //    ID = id;
        //    Name = name;
        //    Description = description;
        //    Source = source;
        //    ImpactObject = impObj;
        //    ConfidentialityBreach = cb;
        //    IntegrityBreach = ib;
        //    AccessibilityBreach = ab;
        //}
        public Threat(ArrayList arrayList)
        {
            //ID = Convert.ToInt32(arrayList[0]); //когда-то ID был интовым
            if (Convert.ToInt32(arrayList[0]) < 10)
            {
                ID = "УБИ.00" + (string)arrayList[0];
            }
            else if (Convert.ToInt32(arrayList[0]) < 100)
            {
                ID = "УБИ.0" + (string)arrayList[0];
            }
            else
            {
                ID = "УБИ." + (string)arrayList[0];
            }
            Name = (string)arrayList[1];
            Description = (string)arrayList[2];
            Source = (string)arrayList[3];
            ImpactObject = (string)arrayList[4];

            if (Convert.ToByte(arrayList[5]) == 1)
                ConfidentialityBreach = "да";
            else
                ConfidentialityBreach = "нет";

            if (Convert.ToByte(arrayList[6]) == 1)
                IntegrityBreach = "да";
            else
                IntegrityBreach = "нет";

            if (Convert.ToByte(arrayList[7]) == 1)
                AccessibilityBreach = "да";
            else
                AccessibilityBreach = "нет";

        }

        public string EqualParams(Threat otherThr)
        {
            string result = "";

            if (this.Name != otherThr.Name)
                result += ("\nНаименование угрозы\nБыло: " + this.Name + "\nСтало: " + otherThr.Name);
            if (this.Description != otherThr.Description)
                result += ("\nОписание\nБыло: " + this.Description + "\nСтало: " + otherThr.Description);
            if (this.Source != otherThr.Source)
                result += ("\nИсточник угрозы\nБыло: " + this.Source + "\nСтало: " + otherThr.Source);
            if (this.ImpactObject != otherThr.ImpactObject)
                result += ("\nОбъект воздействия\nБыло: " + this.ImpactObject + "\nСтало: " + otherThr.ImpactObject);
            if (this.ConfidentialityBreach != otherThr.ConfidentialityBreach)
                result += ("\nНарушение конфиденциальности\nБыло: " + this.ConfidentialityBreach + "\nСтало: " + otherThr.ConfidentialityBreach);
            if (this.IntegrityBreach != otherThr.IntegrityBreach)
                result += ("\nНарушение целостности\nБыло: " + this.IntegrityBreach + "\nСтало: " + otherThr.IntegrityBreach);
            if (this.AccessibilityBreach != otherThr.AccessibilityBreach)
                result += ("\nНарушение доступности\nБыло: " + this.AccessibilityBreach + "\nСтало: " + otherThr.AccessibilityBreach);

            string id = "\nУгроза " + this.ID + ":";
            if (result.Length != 0)
                result = id + result + "\n";
            return result;
        }


        public bool Equals(Threat otherThr)
        {
            if (otherThr == null || this == null) //а то будет эксепшон
                return false;
            if (this.ID != otherThr.ID) //если вдруг сравниваются разные угрозы
                return false;
            else
                return true;
        }

        public override string ToString()
        {
            string info = "Угроза " + ID + "\n";
            info += ("\n\nНаименование угрозы: " + Name);
            info += ("\n\nОписание: " + Description);
            info += ("\n\nИсточник угрозы: " + this.Source);
            info += ("\n\nОбъект воздействия: " + this.ImpactObject);
            info += ("\n\nНарушение конфиденциальности: " + this.ConfidentialityBreach);
            info += ("\n\nНарушение целостности: " + this.IntegrityBreach);
            info += ("\n\nНарушение доступности: " + AccessibilityBreach);
            return  info;
        }
    }
}
